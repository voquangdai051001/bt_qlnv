// messeage
function showMessage(idTag, message) {
  document.getElementById(idTag).innerHTML = message;
}

// Kiểm tra trùng
function kiemTraTrung(taiKhoan, dsnv) {
  var viTri = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == taiKhoan;
  });
  if (viTri != -1) {
    // đã tồn tại
    showMessage("tbTKNV", "Tài khoản đã tồn tại");
    return false;
  } else {
    showMessage("tbTKNV", "");
    return true;
  }
}

// Kiểm tra độ dài
function kiemTraDoDai(value, min, max) {
  if (value.length >= min && value.length <= max) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", `Tài khoản phải từ ${min} đến ${max} ký số`);
    return false;
  }
}

// Kiểm tra tên
function kiemTraTenNV(value) {
  var nameRegex = /^[a-zA-Z\-]+$/;
  if (value.match(nameRegex) == null) {
    showMessage("tbTen", "Phải là chữ");
    return false;
  } else {
    showMessage("tbTen", "");
    return true;
  }
}

// Kiểm tra email
function kiemTraEmail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(email);
  if (isEmail) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ");

    return false;
  }
}

// Kiểm tra mật khẩu
function kiemTraMatKhau(value){
  var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

  if (regularExpression.test(value)){
    showMessage('tbMatKhau', '')
    return true
  }else{
    showMessage('tbMatKhau','Mật khẩu từ 6 đến 10 ký tự và chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt')
    return false
  }
}

// Kiểm tra ngày
function isDate(value) {
  var objDate, mSeconds, month, year;

  if (value.length !== 10) {
    showMessage("tbNgay", "Sai định dạng (mm/dd/yyyy");
    return false;
  }

  if (value.substring(2, 3) !== "/" || value.substring(5, 6) !== "/") {
    showMessage("tbNgay", "Sai định dạng (mm/dd/yyyy");
    return false;
  }

  month = value.substring(0, 2) - 1;
  day = value.substring(3, 5) - 0;
  year = value.substring(6, 10) - 0;

  if (year < 1000 || year > 3000) {
    showMessage("tbNgay", "Sai định dạng (mm/dd/yyyy");
    return false;
  }

  mSeconds = new Date(year, month, day).getTime();

  objDate = new Date();
  objDate.setTime(mSeconds);

  if (
    objDate.getFullYear() !== year ||
    objDate.getMonth() !== month ||
    objDate.getDate() !== day
  ) {
    showMessage("tbNgay", "Sai định dạng (mm/dd/yyyy");
    return false;
  }

  showMessage("tbNgay", "");
  return true;
}

// Kiểm tra lương
function kiemTraLuongCB(value, min, max) {
  if (value >= min && value <= max) {
    showMessage("tbLuongCB", "");
    return true;
  } else {
    showMessage("tbLuongCB", `Lương cơ bản trong khoảng ${min} đến ${max}`);
    return false;
  }
}

// Kiểm tra chức vụ
function kiemTraChucVu(value) {
  if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
    showMessage("tbChucVu", "");
    return true;
  } else {
    showMessage("tbChucVu", "Vui lòng chọn chức vụ");
    return false;
  }
}

// Kiểm tra giờ làm
function kiemTraGioLam(value, min, max) {
  if (value >= min && value <= max) {
    showMessage("tbGiolam", "");
    return true;
  } else {
    showMessage("tbGiolam", "Giờ làm không hợp lệ");
    return false;
  }
}

