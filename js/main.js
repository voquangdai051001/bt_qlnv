var dsnv = [];


// thêm
document.getElementById("btnThemNV").onclick = function () {
  // lấy thông tin từ form
  var nv = layThongTinTuForm();
  // taiKhoan
  var isValid =
    kiemTraTrung(nv.taiKhoan, dsnv) && kiemTraDoDai(nv.taiKhoan, 4, 6);
  // ten
  isValid = isValid & kiemTraTenNV(nv.hoVaTen);
  // email
  isValid = isValid & kiemTraEmail(nv.email);
  // mật khẩu
  isValid = isValid & kiemTraMatKhau(nv.matKhau, 6, 10);
  // ngayLam
  isValid = isValid & isDate(nv.ngayLam);
  // Lương cơ bản
  isValid = isValid & kiemTraLuongCB(nv.luongCoBan, 1000000, 20000000);
  //  Chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucVu);
  // Giờ làm
  isValid = isValid & kiemTraGioLam(nv.gioLam, 80, 200);

  if (isValid) {
    dsnv.push(nv);
    // in ra giao diện
    renderDSNV(dsnv);
    document.getElementById("form_QLNV").reset();
  }
  console.log("dsnvd", dsnv);
};

// Xóa
function xoaNV(id) {
  //   var viTri = -1;
  //   for (var i = 0; i < dsnv.length; i++) {
  //     if (id == dsnv[i].taiKhoan) {
  //       viTri = i;
  //     }
  //   }
  //   dsnv.splice(viTri, 1);
  var viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  dsnv.splice(viTri, 1);
  renderDSNV(dsnv);
}

// Sửa
function suaNV(id) {
  var viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });

  taiKhoan = document.getElementById("tknv").value = dsnv[viTri].taiKhoan;
  hoVaTen = document.getElementById("name").value = dsnv[viTri].hoVaTen;
  email = document.getElementById("email").value = dsnv[viTri].email;
  matKhau = document.getElementById("password").value = dsnv[viTri].matKhau;
  ngayLam = document.getElementById("datepicker").value = dsnv[viTri].ngayLam;
  luongCoBan = document.getElementById("luongCB").value =
    dsnv[viTri].luongCoBan;
  chucVu = document.getElementById("chucvu").value = dsnv[viTri].chucVu;
  gioLam = document.getElementById("gioLam").value = dsnv[viTri].gioLam;
}

// Cập nhật
document.getElementById("btnCapNhat").onclick = function () {
  var nv = layThongTinTuForm();
  var isValid = kiemTraDoDai(nv.taiKhoan, 4, 6);
  // ten
  isValid = isValid & kiemTraTenNV(nv.hoVaTen);
  // email
  isValid = isValid & kiemTraEmail(nv.email);
  // mật khẩu
  isValid = isValid & kiemTraMatKhau(nv.matKhau, 6, 10);
  // ngayLam
  isValid = isValid & isDate(nv.ngayLam);
  // Lương cơ bản
  isValid = isValid & kiemTraLuongCB(nv.luongCoBan, 1000000, 20000000);
  //  Chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucVu);
  // Giờ làm
  isValid = isValid & kiemTraGioLam(nv.gioLam, 80, 200);

  var viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == nv.taiKhoan;
  });

  if (isValid) {
    dsnv[viTri] = nv;
    renderDSNV(dsnv);
  }
};

// ******************** FILTER****
document.getElementById("btnTimNV").onclick = function () {
  var filter_DSNV = [];
  var loai = document.getElementById("searchName").value;
  var content = "";
  if (loai == "Nhân viên xuất sắc" || loai == "Xuất sắc") {
    for (var i = 0; i < dsnv.length; i++) {
      if (dsnv[i].gioLam >= 192) {
        content = dsnv[i];
        filter_DSNV.push(content);
      }
    }
  } else if (loai == "Nhân viên giỏi" || loai == "Giỏi") {
    for (var i = 0; i < dsnv.length; i++) {
      if (dsnv[i].gioLam >= 176 && dsnv[i].gioLam < 192) {
        content = dsnv[i];
        filter_DSNV.push(content);
      }
    }
  } else if (loai == "Nhân viên khá" || loai == "Khá") {
    for (var i = 0; i < dsnv.length; i++) {
      if (dsnv[i].gioLam >= 160 && dsnv[i].gioLam < 176) {
        content = dsnv[i];
        filter_DSNV.push(content);
      }
    }
  } else if (loai == "Nhân viên trung bình" || loai == "Trung bình") {
    for (var i = 0; i < dsnv.length; i++) {
      if (dsnv[i].gioLam < 160) {
        content = dsnv[i];
        filter_DSNV.push(content);
      }
    }
  } else {
    showMessage("tbSearch", "Loại nhân viên không hợp lệ");
  }
  console.log("filter_DSNV: ", filter_DSNV);

  renderDSNV_Filter(filter_DSNV);
};
console.log("dsnvd", dsnv);
// ********************

document.getElementById("btnDong").onclick = function () {
  document.getElementById("form_QLNV").reset();
};
document.getElementById("btnReset").onclick = function () {
  document.getElementById("form_QLNV").reset();
};
