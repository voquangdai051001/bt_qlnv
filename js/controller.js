function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoVaTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;

  return {
    taiKhoan: taiKhoan,
    hoVaTen: hoVaTen,
    email: email,
    matKhau: matKhau,
    ngayLam: ngayLam,
    luongCoBan: luongCoBan,
    chucVu: chucVu,
    gioLam: gioLam,
  };
}

function renderDSNV(dsnv) {
  var showtableDSNV = "";
  for (var i = 0; i < dsnv.length; i++) {
    var contentTr = `<tr>
          <td>${dsnv[i].taiKhoan}</td>
          <td>${dsnv[i].hoVaTen}</td>
          <td>${dsnv[i].email}</td>
          <td>${dsnv[i].ngayLam}</td>
          <td>${dsnv[i].chucVu}</td>
          <td>${tongLuong(dsnv[i].chucVu, dsnv[i].luongCoBan)}</td>
          <td>${xepLoai(dsnv[i].gioLam)}</td>
          <td>
              <button onclick="xoaNV('${
                dsnv[i].taiKhoan
              }')" class="btn btn-danger">Xóa</button>
              <button onclick="suaNV('${
                dsnv[i].taiKhoan
              }')" class="btn btn-warning">Sửa</button>
          </td>
      </tr>`;
    showtableDSNV += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = showtableDSNV;
}

function renderDSNV_Filter(filter_DSNV) {
  var showtableDSNV_filter = "";
  for (var i = 0; i < filter_DSNV.length; i++) {
    var contentTr = `<tr>
        <td>${filter_DSNV[i].taiKhoan}</td>
        <td>${filter_DSNV[i].hoVaTen}</td>
        <td>${filter_DSNV[i].email}</td>
        <td>${filter_DSNV[i].ngayLam}</td>
        <td>${filter_DSNV[i].chucVu}</td>
        <td>${tongLuong(filter_DSNV[i].chucVu, filter_DSNV[i].luongCoBan)}</td>
        <td>${xepLoai(filter_DSNV[i].gioLam)}</td>
        <td>
            <button onclick="xoaNV('${
              filter_DSNV[i].taiKhoan
            }')" class="btn btn-danger">Xóa</button>
            <button onclick="suaNV('${
              filter_DSNV[i].taiKhoan
            }')" class="btn btn-warning">Sửa</button>
        </td>
    </tr>`;
    showtableDSNV_filter += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = showtableDSNV_filter;
}
