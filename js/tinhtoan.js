function tongLuong(chucvu, luongcb) {
  if (chucvu == "Sếp") {
    var tongLuong = luongcb * 3;
  } else if (chucvu == "Trưởng phòng") {
    var tongLuong = luongcb * 2;
  } else if (chucvu == "Nhân viên") {
    var tongLuong = luongcb;
  } else {
    showMessage("tbChucVu", "Vui lòng chọn chức vụ");
  }
  return tongLuong.toLocaleString()
}

function xepLoai(giolam) {
  var loai = "";
  if (giolam >= 192) {
    loai = "Nhân viên xuất sắc";
  } else if (giolam >= 176) {
    loai = "Nhân viên giỏi";
  } else if (giolam >= 160) {
    loai = "Nhân viên khá";
  } else {
    loai = "Nhân viên trung bình";
  }
  return loai;
}


